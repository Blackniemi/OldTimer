# OldTimer
Timer application for marking up gunshots.


## FEATURES

* Gunshot detection with configurable echo delay
* Indoor and outdoor mode
* Voice reporting of last shot time
* Featuring both automatic and manual timer calibration
* Can run on background


## TODO

### Critical
* There appears to be some dispersion in clock rates of different devices. For example: Timer of device 1 may lag behind device 2 by a noticeable amount when clapping after 8 seconds of running. Needs more testing.


### Major
-


### Minor
* Try to separate calibration process into its own Runnable
* Consider implementing rising/falling edge detection to lessen glitches in shot detection
* Carefully add some color to UI (button, screen background?)


#### Tested to work with
HMD Nokia 3 (Nougat, Oreo)
Honor 7 Lite (Nougat)
Nexus 5 (KitKat)
Oneplus 3 (Oreo)
Device X (Oreo)


### Unstudied links on shot detection methods
https://stats.stackexchange.com/questions/41145/simple-way-to-algorithmically-identify-a-spike-in-recorded-errors
https://project.inria.fr/fraclab/files/2016/01/ITS2010Izabela.pdf
