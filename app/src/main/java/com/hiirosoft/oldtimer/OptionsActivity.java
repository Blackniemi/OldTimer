package com.hiirosoft.oldtimer;

import android.content.Intent;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class OptionsActivity extends AppCompatActivity  {

    private static final String TAG = "OptionsActivity";

    private static final int THRESHOLD_UI_MAX = 10;
    private static final int THRESHOLD_UI_MIN = 0;
    private static final int THRESHOLD_MAX = 32000;
    private static final int THRESHOLD_MIN = 22000;
    private static final int THRESHOLD_DEFAULT = 22000;
    private static final int THRESHOLD_UI_DEFAULT = 32000;
    private static final int THRESHOLD_INTERVAL = (THRESHOLD_MAX - THRESHOLD_MIN) / THRESHOLD_UI_MAX;

    private static final int ECHO_MAX = 1550;
    private static final int ECHO_HYSTERESIS_OUTDOOR_DEFAULT = 150;
    private static final int ECHO_HYSTERESIS_INDOOR_DEFAULT = 500;

    private int threshold;
    private int threshold_ui;
    TextView thresholdText;
    SeekBar thresholdVal;

    String lang;
    Spinner languageSpinner;
    ArrayAdapter<CharSequence> languageAdapter;

    private int beepEnabled;
    CheckBox beepSound;

    private int echo_outdoor;
    private int echo_indoor;
    TextView echoOutText;
    TextView echoInText;
    SeekBar echoBarOut;
    SeekBar echoBarIn;

    Button defaultsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.options_menu);

        initTextViews();
        defaultsButton = findViewById(R.id.defaults_button);

        defaultsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showDefaultsDialog();

            }
        });

        thresholdVal.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // TODO Auto-generated method stub
                thresholdText.setText(String.format(Locale.getDefault(), "sensitivity (%d)", progress));

                threshold_ui = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }
        });

        echoBarIn.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                echoInText.setText(String.format(Locale.getDefault(), "echo delay, indoor (%dms)", progress));
                echo_indoor = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        echoBarOut.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                echoOutText.setText(String.format(Locale.getDefault(), "echo delay, outdoor (%dms)", progress));
                echo_outdoor = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void onApplyOptions(View view) {
        /* Function gets called when Apply-button is pressed and gets values of the fields below
         *  and passes them to MainActivity via created intent. */
        Intent intent = new Intent();

        threshold = THRESHOLD_MAX - threshold_ui * THRESHOLD_INTERVAL;
        lang = (String)languageSpinner.getSelectedItem();
        /*
        if (lang.equals("English")) {
            Log.d(TAG, "Language set to English");
        } else if (lang.equals("Android preference")) {
            Log.d(TAG, "Language set to OS preference");
        }
        */

        if(beepSound.isChecked())
            beepEnabled = 1;
        else
            beepEnabled = 0;

        intent.putExtra("threshold", threshold);
        intent.putExtra("lang", lang);
        intent.putExtra("beepEnabled", beepEnabled);
        intent.putExtra("echo_outdoor", echo_outdoor);
        intent.putExtra("echo_indoor", echo_indoor);

        setResult(RESULT_OK, intent);
        finish();
    }


    private void initTextViews(){

        // initialize TextViews
        thresholdText = findViewById(R.id.text_threshold);

        // Get values from MainActivity through Intent
        threshold = getIntent().getIntExtra("threshold", 22000);
        lang = getIntent().getStringExtra("lang");
        beepEnabled = getIntent().getIntExtra("beepEnabled", 1);

        thresholdVal = findViewById(R.id.edit_threshold_bar);
        thresholdVal.setMax(THRESHOLD_UI_MAX);
        threshold_ui = THRESHOLD_UI_MAX - ((threshold - THRESHOLD_MIN) / THRESHOLD_INTERVAL);
        thresholdVal.setProgress(threshold_ui);
        thresholdText.setText(String.format(Locale.getDefault(), "sensitivity (%d)", threshold_ui));

        languageSpinner = findViewById(R.id.voice_language_spinner);
        languageAdapter = ArrayAdapter.createFromResource(this, R.array.language_options, android.R.layout.simple_spinner_dropdown_item);
        languageSpinner.setAdapter(languageAdapter);
        if(lang == null) {
            lang = "English";
        }
        if(lang.equals("English")) {
            languageSpinner.setSelection(0);
        } else {
            languageSpinner.setSelection(1);
        }

        beepSound = findViewById(R.id.beep_sound);
        if(beepEnabled == 1)
            beepSound.setChecked(true);
        else
            beepSound.setChecked(false);

        echo_outdoor = getIntent().getIntExtra("echo_outdoor", ECHO_HYSTERESIS_OUTDOOR_DEFAULT);
        echo_indoor = getIntent().getIntExtra("echo_indoor", ECHO_HYSTERESIS_INDOOR_DEFAULT);
        echoOutText = findViewById(R.id.text_echo_outdoor);
        echoInText = findViewById(R.id.text_echo_indoor);
        echoBarOut = findViewById(R.id.edit_echo_bar_outdoor);
        echoBarIn = findViewById(R.id.edit_echo_bar_indoor);
        echoBarOut.setMax(ECHO_MAX);
        echoBarIn.setMax(ECHO_MAX);
        echoOutText.setText(String.format(Locale.getDefault(), "echo delay, outdoor (%dms)", echo_outdoor));
        echoInText.setText(String.format(Locale.getDefault(), "echo delay, indoor (%dms)", echo_indoor));
        echoBarOut.setProgress(echo_outdoor);
        echoBarIn.setProgress(echo_indoor);
    }

    private void showDefaultsDialog(){
        DefaultsDialog yesNoAlert = DefaultsDialog.newInstance("Data to Send");
        yesNoAlert.show(getFragmentManager(), "yesNoAlert");

        yesNoAlert.setOnYesNoClick(new DefaultsDialog.OnYesNoClick() {
            @Override
            public void onYesClicked() {

                thresholdVal.setProgress(THRESHOLD_UI_DEFAULT);
                languageSpinner.setSelection(1);
                beepSound.setChecked(true);
                echoBarOut.setProgress(ECHO_HYSTERESIS_OUTDOOR_DEFAULT);
                echoBarIn.setProgress(ECHO_HYSTERESIS_INDOOR_DEFAULT);

                Toast.makeText(getApplicationContext(), "Settings reset.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNoClicked() {

            }
        });
    }
}
