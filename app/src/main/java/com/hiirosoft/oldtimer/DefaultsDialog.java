package com.hiirosoft.oldtimer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

public class DefaultsDialog extends DialogFragment {

    public static final String TITLE = "dataKey";
    private OnYesNoClick yesNoClick;

    private static final String dialogMsg = "You will lose current settings and load app defaults.\n\n" +
            "Continue?";

    public static DefaultsDialog newInstance(String dataToShow ) {
        DefaultsDialog frag = new DefaultsDialog();
        Bundle args = new Bundle();
        args.putString(TITLE, dataToShow);
        frag.setArguments(args);
        return frag;
    }

    public void setOnYesNoClick(OnYesNoClick yesNoClick) {
        this.yesNoClick = yesNoClick;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setMessage(dialogMsg)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(yesNoClick != null)
                            yesNoClick.onNoClicked();
                    }
                })

                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(yesNoClick != null)
                            yesNoClick.onYesClicked();
                    }
                });
        Dialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        return dialog;

    }

    public interface OnYesNoClick{
        void onYesClicked();
        void onNoClicked();
    }
}