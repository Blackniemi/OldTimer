package com.hiirosoft.oldtimer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

public class CalibrateDialog extends DialogFragment {

    public static final String TITLE = "dataKey";
    private OnYesNoClick yesNoClick;

    private static final String dialogMsg = "After clicking accept, wait 5 seconds for speaker to calibrate. " +
            "Then fire a single shot to calibrate timer sensitivity.\n\n" +
            "Note: Consecutive uses of calibration require app restart\n\n\n" +
            "Start calibration?";

    public static CalibrateDialog newInstance(String dataToShow ) {
        CalibrateDialog frag = new CalibrateDialog();
        Bundle args = new Bundle();
        args.putString(TITLE, dataToShow);
        frag.setArguments(args);
        return frag;
    }

    public void setOnYesNoClick(OnYesNoClick yesNoClick) {
        this.yesNoClick = yesNoClick;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setMessage(dialogMsg)
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(yesNoClick != null)
                            yesNoClick.onNoClicked();
                    }
                })

                .setNegativeButton("Indoor", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(yesNoClick != null)
                            yesNoClick.onYesClicked(true);
                    }
                })

                .setPositiveButton("Outdoor", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(yesNoClick != null)
                            yesNoClick.onYesClicked(false);
                    }
                });
        Dialog dialog = builder.create();

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        return dialog;

    }

    public interface OnYesNoClick{
        void onYesClicked(boolean isIndoor);
        void onNoClicked();
    }
}