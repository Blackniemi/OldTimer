package com.hiirosoft.oldtimer;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.hiirosoft.waveform.WaveformView;

import java.util.LinkedList;
import java.util.Locale;

import static java.lang.System.currentTimeMillis;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "OldTimer";
    private static final int REQUEST_RECORD_AUDIO = 13;
    SharedPreferences sharedPrefs;

    Vibrator vibra;

    private static final long TIMER_DELTATIME_MILLIS = 20;
    private static final long PRETIME_MILLIS = 5000;
    private static final int WHITE_SCREEN_TIME_MILLIS_DEFAULT = 100;

    private static final int THRESHOLD_DEFAULT = 22000;
    private static final int THRESHOLD_UI_MAX = 10;
    private static final int THRESHOLD_MAX = 32000;
    private static final int THRESHOLD_MIN = 22000;
    private static final int THRESHOLD_INTERVAL = (THRESHOLD_MAX - THRESHOLD_MIN) / THRESHOLD_UI_MAX;

    /* audio level threshold for shot detection */
    private int threshold = THRESHOLD_DEFAULT;

    private static final int ECHO_HYSTERESIS_OUTDOOR_DEFAULT = 180;
    private static final int ECHO_HYSTERESIS_INDOOR_DEFAULT = 500;
    private static final int ECHO_MINIMUM_MILLIS = 180;
    private static final int ECHO_ADDITIONAL_SAFEROOM_MILLIS = 100;
    /* Temporal hysteresis in milliseconds to eliminate multiple detections on one shot */
    private long ts_hysteresis = ECHO_MINIMUM_MILLIS;
    private int echo_hyst_outdoor = ECHO_HYSTERESIS_OUTDOOR_DEFAULT;
    private int echo_hyst_indoor = ECHO_HYSTERESIS_INDOOR_DEFAULT;

    private String language;

    private int beepEnabled = 1;

    public enum State {
        IDLE,
        PREWAIT,
        RECORDING,
        CALIBRATING
    }

    public enum CalibStage {
        CALIBRATING_SPEAKER,
        CALIBRATING_SHOT_SENS,
        CALIBRATING_CLEAR_DELAY,
        CALIBRATING_SHOT_ECHO
    }

    public enum AmbientSetting {
        OUTDOOR,
        INDOOR
    }

    State state = State.IDLE;
    CalibStage calibStage = CalibStage.CALIBRATING_SPEAKER;
    AmbientSetting ambientSetting = AmbientSetting.OUTDOOR;

    private boolean recordedOnBackground = false;

    AudioManager audioManager;
    ToneGenerator toneGen;
    private int audioMaxVolume;
    private int appVolume = 15;
    private int offAppVolume;

    private RecordingThread mRecordingThread;

    private LinkedList<short[]>a_data = new LinkedList<>();
    private LinkedList<Long> shots_ts = new LinkedList<>();
    private LinkedList<String> shots_strs = new LinkedList<>();
    private int shot_count;

    ArrayAdapter shotListAdapter;

    private WaveformView mRealtimeWaveformView;
    TextView timerTextView;
    ToggleButton timerToggleButton;
    ToggleButton clearToggleButton;
    ToggleButton optionsToggleButton;
    ToggleButton ambientToggleButton;

    long pretime_start_ts = 0;
    long time1 = 0;

    TextToSpeech textSpeaker;
    private boolean voiceEnabled;
    private boolean flashEnabled;

    TextView whiteScreenNum;
    SurfaceView whiteScreen;
    private long whiteScreenTimer = 0;

    ProgressBar calibBar;
    TextView topText;
    TextView fireText;
    int calibVol = 1;
    long calibShotVal = 0;
    long calibShotTs = 0;
    long calibShotEcho = 0;

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {

            //Log.v(LOG_TAG, "I'm still running, kjeh..");
            if (state == State.PREWAIT) {

                long pretimer = pretime_start_ts + PRETIME_MILLIS - System.currentTimeMillis();
                int secs = (int) (pretimer / 1000);
                long millis = pretimer - (secs * 1000);

                timerTextView.setText(String.format("%02d.%02d", secs, (int) (millis / 10)));

                if(pretimer <= 0) {
                    state = State.RECORDING;
                    time1 = currentTimeMillis();
                    timerTextView.setTextColor(getResources().getColor(R.color.timerGreen));
                    topText.setTextColor(getResources().getColor(R.color.timerGreen));
                    topText.setText("FIRE");
                    startAudioRecordingSafe();

                    if(beepEnabled == 1)
                        toneGen.startTone(ToneGenerator.TONE_DTMF_D, 1000);
                }
            } else if (state == State.RECORDING) {

                long timer = currentTimeMillis() - time1;
                int secs = (int) (timer / 1000);
                timer = timer - (secs * 1000);

                timerTextView.setText(String.format("%02d.%02d", secs, (int) (timer / 10)));

                /* Update ShotList */
                if(shots_ts.size() > shots_strs.size()) {
                    long ts = shots_ts.getLast();
                    int s = (int) ts / 1000;
                    int ms = Math.round((ts - (s * 1000))/10);
                    String shot_str = String.format("%d:    %d.%02d", shots_ts.size(), s, ms);
                    shots_strs.add(shot_str);
                    shotListAdapter.notifyDataSetChanged();
                    whiteScreenNum.setText(String.valueOf(shot_count));
                }

                if(shots_strs.size() > 0) {
                    topText.setVisibility(View.GONE);
                }

                if (flashEnabled) {
                    if (whiteScreenTimer > 0) {
                        whiteScreenTimer = whiteScreenTimer - TIMER_DELTATIME_MILLIS;
                        if (whiteScreen.getVisibility() == View.GONE)
                        {
                            whiteScreen.setVisibility(View.VISIBLE);
                            whiteScreenNum.setVisibility(View.VISIBLE);
                            whiteScreenNum.setText(String.valueOf(shot_count));
                        }
                    } else {
                        ClearWhiteScreen();
                    }
                }
            } else if (state == State.CALIBRATING) {

                if(calibStage == CalibStage.CALIBRATING_SPEAKER) {

                    if(shots_ts.size() < 1) {

                        if(calibVol == audioMaxVolume) {
                            appVolume = calibVol;
                            calibStage = CalibStage.CALIBRATING_CLEAR_DELAY;
                            fireText.setText("FIRE");
                            Log.v(LOG_TAG, "speaker calibrated to max volume " + appVolume);
                            timerHandler.postDelayed(this, 800);
                            return;
                        }
                        else
                        {
                            calibVol++;
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, calibVol, 0);
                            Log.v(LOG_TAG, "incremented to " + calibVol);
                        }
                    }
                    else
                    {
                        appVolume = calibVol - 2;
                        toneGen.stopTone();
                        calibStage = CalibStage.CALIBRATING_CLEAR_DELAY;
                        fireText.setText("FIRE");
                        Log.v(LOG_TAG, "speaker calibrated to " + appVolume);
                        timerHandler.postDelayed(this, 800);
                        return;
                    }
                    timerHandler.postDelayed(this, 300);
                    return;
                }
                else if(calibStage == CalibStage.CALIBRATING_CLEAR_DELAY) {
                    ClearTimerData();
                    calibShotVal = 0;
                    calibShotTs = 0;
                    calibShotEcho = 0;
                    timerHandler.postDelayed(this, TIMER_DELTATIME_MILLIS);
                    calibStage = CalibStage.CALIBRATING_SHOT_SENS;
                    return;
                }
                else if (calibStage == CalibStage.CALIBRATING_SHOT_SENS) {

                    int showFireText = (int)(currentTimeMillis() / 500) % 2;
                    if(showFireText == 0) {
                        fireText.setVisibility(View.VISIBLE);
                    } else {
                        fireText.setVisibility(View.INVISIBLE);
                    }

                    if(shots_ts.size() > 0) {
                        int scaledProgress = (int)(((calibShotVal - THRESHOLD_MIN)*100) / (THRESHOLD_MAX - THRESHOLD_MIN));
                        calibBar.setProgress(scaledProgress);
                        timerTextView.setVisibility(View.VISIBLE);

                        threshold = (scaledProgress / 10) * THRESHOLD_INTERVAL + THRESHOLD_MIN;
                        if(threshold > THRESHOLD_MAX)
                            threshold = THRESHOLD_MAX;
                        if(threshold < THRESHOLD_MIN)
                            threshold = THRESHOLD_MIN;

                        calibStage = CalibStage.CALIBRATING_SHOT_ECHO;
                        Log.v(LOG_TAG, "CALIBRATION SHOT DETECTED! (" + calibShotVal + ")");
                    }
                }
                else if(calibStage == CalibStage.CALIBRATING_SHOT_ECHO) {

                    Log.v(LOG_TAG, "awaiting echo settling.. (" + (currentTimeMillis() - shots_ts.getFirst()) + ")");
                    if(calibShotEcho > 0) {

                        /* Add additional echo delay to be on the safe side */
                        calibShotEcho += ECHO_ADDITIONAL_SAFEROOM_MILLIS;
                        if(calibShotEcho < ECHO_MINIMUM_MILLIS)
                            calibShotEcho = ECHO_MINIMUM_MILLIS;

                        timerHandler.removeCallbacks(this);
                        mRecordingThread.stopRecording();

                        int newSens = THRESHOLD_UI_MAX - ((threshold - THRESHOLD_MIN) / THRESHOLD_INTERVAL);
                        if(newSens < 0)
                            newSens = 0;
                        if(newSens > THRESHOLD_UI_MAX)
                            newSens = THRESHOLD_UI_MAX;

                        String scaledVol = String.format("%02.0f", ((float)appVolume / (float)audioMaxVolume)*100);

                        if(ambientSetting == AmbientSetting.OUTDOOR) {
                            echo_hyst_outdoor = (int)calibShotEcho;
                        }
                        else {
                            echo_hyst_indoor = (int)calibShotEcho;
                        }

                        Toast.makeText(MainActivity.this, "Calibration done." +
                                        "\n\nSpeaker safe volume set to " + scaledVol + "%" +
                                        "\nSensitivity set to " + String.valueOf(newSens) +
                                        "\nEcho set to " + String.valueOf(calibShotEcho) + "ms",
                                        Toast.LENGTH_LONG).show();
                        //Toast.makeText(MainActivity.this, "new threshold " + String.valueOf(threshold), Toast.LENGTH_SHORT).show();

                        HideCalibUI();
                        UpdateAmbientSetting(false);

                        if(voiceEnabled)
                            textSpeaker.speak("calibration complete!", TextToSpeech.QUEUE_FLUSH, null);

                        state = State.IDLE;
                        return;
                    }
                }

            }

            timerHandler.postDelayed(this, TIMER_DELTATIME_MILLIS);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(LOG_TAG, "SST onCreate()");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            CheckPermissions();
        }

        LoadAppPreferences();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        audioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
        vibra = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        audioMaxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        offAppVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        toneGen = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

        whiteScreenNum = findViewById(R.id.white_screen_number);
        whiteScreenNum.setVisibility(View.GONE);
        whiteScreen = findViewById(R.id.white_screen);
        whiteScreen.setVisibility(View.GONE);

        calibBar = findViewById(R.id.calib_progressbar);
        topText = findViewById(R.id.top_text);
        fireText = findViewById(R.id.fire_text);
        HideCalibUI();

        final ListView shotList = findViewById(R.id.shotList);

        shotListAdapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1, shots_strs) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = view.findViewById(android.R.id.text1);
                text.setTextColor(Color.WHITE);
                return view;
            }
        };

        shotList.setAdapter(shotListAdapter);

        mRealtimeWaveformView = findViewById(R.id.waveformView);
        mRecordingThread = new RecordingThread(new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {

                /* Copy samples to local variable */
                /*short[] tmp = new short[data.length];
                int i = 0;
                for (short s : data) {
                    tmp[i++] = s;
                }
                a_data.add(tmp);*/
                //Log.v(LOG_TAG, Arrays.toString(data));

                mRealtimeWaveformView.setSamples(data);

                /* Analyze "data" and detect gunshots */
                CheckShots(data);
            }
        });

        timerTextView = findViewById(R.id.timerTextView);
        timerToggleButton = findViewById(R.id.timeToggleButton);
        clearToggleButton = findViewById(R.id.clearToggleButton);
        optionsToggleButton = findViewById(R.id.optionsToggleButton);
        ambientToggleButton = findViewById(R.id.ambientToggleButton);

        UpdateAmbientSetting(false);

        timerToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (state == State.IDLE) {

                    state = State.PREWAIT;

                    ClearTimerData();

                    /* Start timer thread */
                    pretime_start_ts = currentTimeMillis();
                    time1 = currentTimeMillis();
                    timerTextView.setTextColor(getResources().getColor(R.color.timerRed));
                    timerToggleButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.timer_button_clicked));
                    timerHandler.postDelayed(timerRunnable, 0);

                    topText.setTextColor(getResources().getColor(R.color.timerRed));
                    topText.setText("WAIT");
                    topText.setVisibility(View.VISIBLE);

                } else {

                    timerToggleButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.timer_button));

                    topText.setVisibility(View.GONE);

                    if(state == State.CALIBRATING)
                        CancelCalibration();

                    /* Stop timer and recording threads */
                    timerHandler.removeCallbacks(timerRunnable);
                    mRecordingThread.stopRecording();
                    //writeToFile(a_data);

                    ClearWhiteScreen();

                    /* Do nothing if there were no shots */
                    if(shots_ts.size() == 0) {
                        state = State.IDLE;
                        return;
                    }

                    /* Time of the final shot */
                    long ts = shots_ts.getLast();
                    int s = (int) ts / 1000;
                    int ms = Math.round((ts - (s * 1000))/10);

                    String last_shot_str = String.format("%02d.%02d", s, ms);
                    timerTextView.setText(last_shot_str);

                    if (voiceEnabled)
                        textSpeaker.speak(last_shot_str.replaceFirst("^0+(?!$)", ""), TextToSpeech.QUEUE_FLUSH, null);

                    /*StringBuilder shots_string = new StringBuilder();
                    Iterator<?> it = shots_ts.descendingIterator();

                    while (it.hasNext()) {
                        shots_string.append(it.next() + "\n");
                    }

                     Toast.makeText(MainActivity.this, "SHOTS\n" + shots_string + "\n(" + shots_ts.size() + " shots)", Toast.LENGTH_LONG).show();
                    */
                    state = State.IDLE;
                }
            }
        });

        clearToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch(state) {
                    case IDLE:
                        ClearTimerData();
                        timerTextView.setText("00.00");
                        break;

                    case CALIBRATING:
                        timerHandler.removeCallbacks(timerRunnable);
                        mRecordingThread.stopRecording();
                        CancelCalibration();
                        state = State.IDLE;
                        break;
                }
            }
        });

        optionsToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LaunchSettingsMenu();
            }
        });

        ambientToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateAmbientSetting(true);
            }
        });

        flashEnabled = true;
        voiceEnabled = true;
        textSpeaker = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    if (language.equals("English")) {
                        textSpeaker.setLanguage(Locale.UK);
                    } else if (language.equals("Android preference")) {
                        textSpeaker.setLanguage(Locale.getDefault());
                    }
                }
            }
        });
    }

    @Override
    protected  void onStart() {
        super.onStart();
        Log.i(LOG_TAG, "SST onStart()");
    }

    @Override
    protected  void onResume() {
        super.onResume();
        Log.i(LOG_TAG, "SST onResume()");

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        if(!recordedOnBackground) {
            /* Set our app specific volume when app in focus */
            offAppVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            Log.v(LOG_TAG, "Volume restored to " + appVolume + ". Updated offAppVolume " + offAppVolume);

            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, appVolume, 0);
        }
    }

    @Override
    protected  void onPause() {
        super.onPause();
        Log.i(LOG_TAG, "SST onPause()");

        appVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        /* If we are not in active state on background, restore system volume */
        if(state == State.IDLE) {
            Log.v(LOG_TAG, "volume set to offAppVolume " + offAppVolume);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, offAppVolume, 0);
            recordedOnBackground = false;
        }
        else {
            Log.v(LOG_TAG, "volume unchanged.");
            recordedOnBackground = true;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(LOG_TAG, "SST onStop()");
        StoreAppPreferences();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(LOG_TAG, "SST onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "SST onDestroy()");
    }

    private void CheckShots(short[] data) {
        boolean found = false;

        for(short s : data) {
            if(s > threshold && !found) {
                Log.v(LOG_TAG, "SHOTVAL" + String.valueOf(s));
                found = true;
            }
        }

        if(found) {
            MarkShot();
            if(state == State.CALIBRATING && calibStage == CalibStage.CALIBRATING_SHOT_SENS) {
                if(calibShotVal == 0) {
                    for(short s : data) {
                        if(s > threshold) {
                            calibShotVal = s;
                            calibShotTs = currentTimeMillis();
                        }
                    }
                }
            }
        }

        if(state == State.CALIBRATING && calibStage == CalibStage.CALIBRATING_SHOT_ECHO) {
            if(calibShotEcho == 0){
                short max = 0;
                for(short s : data) {
                    if(s > max) {
                        max = s;
                    }
                    /*if(s < calibShotVal) {
                        calibShotEcho = (currentTimeMillis() - calibShotTs);
                    }*/
                }
                if(max < calibShotVal) {
                    calibShotEcho = (currentTimeMillis() - calibShotTs);
                    Log.v(LOG_TAG, "calib_ts " + calibShotTs + ",  echo_duration " + calibShotEcho);
                }
                else {
                    Log.v(LOG_TAG, "still too high");
                }
            }
        }
    }

    private void MarkShot() {
        long millis = currentTimeMillis() - time1;
        if(shots_ts.size() == 0) {
            shots_ts.add(millis);
            shot_count++;
        }
        else if(millis - shots_ts.getLast() > ts_hysteresis) {
            shots_ts.add(millis);
            shot_count++;
        }

        if(flashEnabled) {
            whiteScreenTimer = WHITE_SCREEN_TIME_MILLIS_DEFAULT;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        switch (item.getItemId()) {
            case R.id.action_settings:
                LaunchSettingsMenu();
                return true;

            case R.id.action_toggle_voice:
                item.setChecked(!item.isChecked());
                voiceEnabled = item.isChecked();
                return true;

            case R.id.action_toggle_flash:
                item.setChecked(!item.isChecked());
                flashEnabled = item.isChecked();
                return true;

            case R.id.action_calibrate:
                showYesNoDialog();
                return true;

        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /* If data coming from SettingsActivity exists, the method sets variable values accordingly.
          If a value doesn't exist, the previous value for the variable is selected. */
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK){

            threshold = (short) data.getIntExtra("threshold", threshold);
            language = data.getStringExtra("lang");
            beepEnabled = data.getIntExtra("beepEnabled", beepEnabled);

            if (language.equals("English")) {
                textSpeaker.setLanguage(Locale.UK);
            } else if (language.equals("Android preference")) {
                textSpeaker.setLanguage(Locale.getDefault());
            }

            echo_hyst_outdoor = data.getIntExtra("echo_outdoor", ECHO_HYSTERESIS_OUTDOOR_DEFAULT);
            echo_hyst_indoor = data.getIntExtra("echo_indoor", ECHO_HYSTERESIS_INDOOR_DEFAULT);
            UpdateAmbientSetting(false);
        }
    }

    private void startAudioRecordingSafe() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.startRecording();
        } else {
            requestMicrophonePermission();
        }
    }

    @TargetApi(23)
    private void CheckPermissions() {
        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
            return;
        }
    }

    private void requestMicrophonePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.RECORD_AUDIO)) {
            // Show dialog explaining why we need record audio
            Snackbar.make(mRealtimeWaveformView, "Microphone access is required in order to record audio",
                    Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                            android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
                }
            }).show();
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{
                    android.Manifest.permission.RECORD_AUDIO}, REQUEST_RECORD_AUDIO);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_RECORD_AUDIO && grantResults.length > 0 &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mRecordingThread.stopRecording();
        }
    }

    private void StoreAppPreferences() {
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("lang", language);
        editor.putInt("threshold", threshold);
        editor.putInt("beepEnabled", beepEnabled);
        editor.putInt("appVolume", appVolume);
        editor.putInt("echoOutdoor", echo_hyst_outdoor);
        editor.putInt("echoIndoor", echo_hyst_indoor);
        editor.apply();
    }

    private void LoadAppPreferences() {
        sharedPrefs = getPreferences(Context.MODE_PRIVATE);
        language = sharedPrefs.getString("lang", "English");
        threshold = sharedPrefs.getInt("threshold", THRESHOLD_DEFAULT);
        beepEnabled = sharedPrefs.getInt("beepEnabled", beepEnabled);
        appVolume = sharedPrefs.getInt("appVolume", appVolume);
        echo_hyst_outdoor = sharedPrefs.getInt("echoOutdoor", echo_hyst_outdoor);
        echo_hyst_indoor = sharedPrefs.getInt("echoIndoor", echo_hyst_indoor);
    }

    private void showYesNoDialog(){
        CalibrateDialog yesNoAlert = CalibrateDialog.newInstance("Data to Send");
        yesNoAlert.show(getFragmentManager(), "yesNoAlert");

        yesNoAlert.setOnYesNoClick(new CalibrateDialog.OnYesNoClick() {
            @Override
            public void onYesClicked(boolean isIndoor) {
                new asyncTaskUpdateProgress().execute();
                RevealCalibUI();
                timerTextView.setVisibility(View.INVISIBLE);
                state = State.CALIBRATING;
                calibStage = CalibStage.CALIBRATING_SPEAKER;
                threshold = THRESHOLD_DEFAULT;
                if(isIndoor)
                    ambientSetting = AmbientSetting.INDOOR;
                else
                    ambientSetting = AmbientSetting.OUTDOOR;
                UpdateAmbientSetting(false);
                calibVol = 1;
                calibShotVal = 0;
                calibShotTs = 0;
                calibShotEcho = 0;
                ts_hysteresis = 0;
                ClearTimerData();
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, calibVol, 0);
                startAudioRecordingSafe();
                timerHandler.postDelayed(timerRunnable, 300);

                toneGen.startTone(ToneGenerator.TONE_DTMF_D, 4500);
            }

            @Override
            public void onNoClicked() {
                Toast.makeText(MainActivity.this, "calibration canceled.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public class asyncTaskUpdateProgress extends AsyncTask<Void, Integer, Void> {

        int progress;

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //buttonStart.setClickable(true);
        }

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            progress = 0;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // TODO Auto-generated method stub
            calibBar.setProgress(values[0]);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
/*            while(progress<100){
                progress++;
                publishProgress(progress);
                SystemClock.sleep(100);
            }*/
            return null;
        }
    }

    private void HideCalibUI() {
        calibBar.setVisibility(View.INVISIBLE);
        topText.setVisibility(View.INVISIBLE);
        fireText.setVisibility(View.INVISIBLE);
    }

    private void RevealCalibUI() {
        //calibBar.setVisibility(View.VISIBLE);
        topText.setVisibility(View.VISIBLE);
        topText.setTextColor(getResources().getColor(R.color.timerGreen));
        topText.setText("CALIBRATE");
        fireText.setVisibility(View.VISIBLE);
        fireText.setText("WAIT");
    }

    private void ClearTimerData() {
        /* Clear data vars */
        a_data.clear();
        shots_ts.clear();
        shots_strs.clear();
        shotListAdapter.notifyDataSetChanged();
        shot_count = 0;
        whiteScreenTimer = 0;
    }

    private void LaunchSettingsMenu() {
        // Create a new intent and pass parameters for settingsActivity when Settings button is clicked
        Intent intent = new Intent(this, OptionsActivity.class);
        intent.putExtra("threshold", threshold);
        intent.putExtra("lang", language);
        intent.putExtra("beepEnabled", beepEnabled);
        intent.putExtra("echo_outdoor", echo_hyst_outdoor);
        intent.putExtra("echo_indoor", echo_hyst_indoor);
        startActivityForResult(intent, 1);
    }

    private void CancelCalibration() {
        timerTextView.setVisibility(View.VISIBLE);
        timerTextView.setText("00.00");
        HideCalibUI();
        threshold = sharedPrefs.getInt("threshold", THRESHOLD_DEFAULT);
        Toast.makeText(MainActivity.this, "Calibration cancelled.", Toast.LENGTH_LONG).show();
    }

    private void ClearWhiteScreen() {
        whiteScreen.setVisibility(View.GONE);
        whiteScreenNum.setVisibility(View.GONE);
        whiteScreenTimer = 0;
    }

    private void UpdateAmbientSetting(boolean doToggle) {

        /* Flip setting if required */
        if(doToggle) {
            if(ambientSetting == AmbientSetting.OUTDOOR)
                ambientSetting = AmbientSetting.INDOOR;
            else
                ambientSetting = AmbientSetting.OUTDOOR;

            vibra.vibrate(100);
        }

        /* Refresh latest values */
        switch (ambientSetting) {
            case OUTDOOR:
                ambientToggleButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ui_ambient_button_outdoor));
                ts_hysteresis = echo_hyst_outdoor;
                //Toast.makeText(this, "out " + ts_hysteresis, Toast.LENGTH_SHORT).show();
                break;

            case INDOOR:
                ambientToggleButton.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ui_ambient_button_indoor));
                ts_hysteresis = echo_hyst_indoor;
                //Toast.makeText(this, "in " + ts_hysteresis, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}

/*
    private void writeToFile(LinkedList<short[]> data)
    {
        // Format filename by date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US);
        Date now = new Date();
        String fileName = formatter.format(now) + ".csv";

        final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS + "/timer_logs/");

        if(!path.exists())
        {
            path.mkdirs();
        }

        final File file = new File(path, fileName);

        try
        {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);

            StringBuilder string = new StringBuilder();

            for(int t=0; t<a_data.size(); t++) {
                short[] tmp = a_data.get(t);
                for(int i=0; i<tmp.length; i++) {
                    string.append(Short.toString(tmp[i]) + "\n");
                }
            }

            myOutWriter.append(string);
            myOutWriter.close();

            fOut.flush();
            fOut.close();

            Log.v(LOG_TAG, "sample groups:" + a_data.size() + "  total samples:" + (a_data.size()*a_data.get(0).length));
            Log.v(LOG_TAG, "Samples saved to log file. " + file.getPath());
            Toast.makeText(MainActivity.this, "Samples saved to log file. " + file.getPath() + " (" + (file.length() / 1024) + "Kb)", Toast.LENGTH_LONG).show();
        }
        catch (IOException e)
        {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
*/
