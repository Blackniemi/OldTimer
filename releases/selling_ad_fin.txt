################
     MAINOS
 ANDROID TIMERI
################

Oon työstänyt simppeliä laukauslaskuria Android luureille. Löytyy Play Storesta hakusanalla "Simple Shot Timer". Appi on tällä hetkellä betassa, kaikki featuret mukana ja tiedossa olevat bugit on liiskattu. Appi pyrkii imitoimaan kaupallisen timerin toimintaa niin hyvin että sitä voi käyttää reenaamiseen.

Vaatii vähintään Android 4.4 (KitKat) tasoisen luurin. Testattu jo:
Nexus 5 (KitKat)
HMD Nokia 3 (Nougat)
Honor 7 Lite (Nougat)
Oneplus 3 (Oreo)

Promokoodit nopeimmille:
5CBZCSQRXL2JUU0529LDQ4V
9GSTCL891KW4P9HV5NE4G5D
6Z857J1A5W3Z93R09LD3QMK
